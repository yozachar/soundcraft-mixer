package com.yozachar.apps.soundcraftmixer

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.InetAddresses
import android.net.NetworkCapabilities
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import java.net.Inet4Address

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPref = getSharedPreferences("appData", Context.MODE_PRIVATE)
        val newUrl = sharedPref.getString("NEW_URL", "")
        if (newUrl.isNullOrEmpty() || newUrl.startsWith("file://")) {
            setContent { ConnectWebView() }
        } else {
            val intent = Intent(this, WebLoader::class.java).apply {
                putExtra("TARGET_URL", newUrl)
            }
            startActivity(intent)
            finish()
        }
    }
}

fun getWiFiAddress(context: Context): String? {
    val connMgr = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
    val network = connMgr.activeNetwork
    val networkCapabilities = connMgr.getNetworkCapabilities(network)
    if (networkCapabilities?.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) == true) {
        val linkProperties = connMgr.getLinkProperties(network)
        val ipAddress =
            linkProperties?.linkAddresses?.filter { it.address is Inet4Address }?.map { it.address }
                ?.lastOrNull()?.hostAddress
        // Log.d("debug", "++++++++++++ $ipAddress ++++++++++++")
        return ipAddress
    }
    return null
}

class ConditionalInputs(
    val fieldLabel: String,
    val fieldPlaceholder: String,
    val inputPrefix: String,
    val exampleText: String,
)


@Composable
fun ConnectWebView() {
    var ipAddress by remember { mutableStateOf("") }
    var textValue by remember { mutableStateOf("") }
    var mixerId by remember { mutableIntStateOf(10) }
    var isEnterFullAddressPressed by remember { mutableStateOf(false) }

    val ipv4Network =
        (getWiFiAddress(LocalContext.current) ?: "192.168.0.1").substringBeforeLast(".")
    // Log.d("debug", ipv4Network)

    // get theme info
    val isDarkTheme =
        LocalContext.current.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
    val textColor = if (isDarkTheme) Color.White else Color.Black
    val backgroundColor = if (isDarkTheme) Color.Black else Color.White
    val disableBtnColor = if (isDarkTheme) Color(0xFF333333) else Color(0xFFAAAAAA)

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(0.dp)
            .background(color = backgroundColor),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                if (isEnterFullAddressPressed) "Connect to the digital mixer's LAN" else "MixerID is the last part of the mixer's IP address",
                style = MaterialTheme.typography.bodyMedium,
                color = textColor,
                textAlign = TextAlign.Center
            )

            Spacer(modifier = Modifier.height(16.dp))

            val cip = ConditionalInputs(
                fieldLabel = if (isEnterFullAddressPressed) "Enter IP Address" else "Enter MixerID (0 - 255)",
                fieldPlaceholder = if (isEnterFullAddressPressed) "192.168.0.10" else mixerId.toString(),
                inputPrefix = if (isEnterFullAddressPressed) "" else "$ipv4Network.",
                exampleText = if (isEnterFullAddressPressed) "eg. 192.168.0.10" else "eg. XX part in 192.168.0.XX",
            )

            OutlinedTextField(
                value = textValue,
                onValueChange = { it ->
                    if (isEnterFullAddressPressed) {
                        textValue = it
                        ipAddress = it
                    } else {
                        val newText = it.filter { it.isDigit() }.take(3)
                        textValue = newText
                        val newValue = if (newText.isEmpty()) 10 else newText.toInt()
                        mixerId = if (newValue in 0..255) newValue else 10
                    }
                },
                // better skip 0, 1 & 255
                label = { Text(cip.fieldLabel) },
                keyboardOptions = KeyboardOptions.Default.copy(
                    keyboardType = KeyboardType.Number, imeAction = ImeAction.Done
                ),
                placeholder = { Text(cip.fieldPlaceholder, color = Color.Gray) },
                prefix = { Text(text = cip.inputPrefix) },
                supportingText = { Text(text = cip.exampleText, color = Color.Gray) },
            )

            Spacer(modifier = Modifier.height(16.dp))

            val targetPageUrl =
                if (isEnterFullAddressPressed) ipAddress else "$ipv4Network.$mixerId"
            val context = LocalContext.current
            // Log.d("debug", targetPageUrl)

            Row(
                modifier = Modifier.padding(16.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Button(
                    onClick = { isEnterFullAddressPressed = !isEnterFullAddressPressed },
                    modifier = Modifier.width(150.dp)
                ) {
                    Text(if (isEnterFullAddressPressed) "Enter MixerID" else "Enter IPv4 address")
                }

                Spacer(modifier = Modifier.width(16.dp))

                Button(
                    onClick = {
                        val intent = Intent(context, WebLoader::class.java).apply {
                            putExtra("TARGET_URL", targetPageUrl)
                        }
                        context.startActivity(intent)
                    },
                    enabled = InetAddresses.isNumericAddress(targetPageUrl),
                    colors = ButtonDefaults.buttonColors(disabledContainerColor = disableBtnColor)
                ) {
                    Icon(Icons.AutoMirrored.Filled.ArrowForward, "Connect")
                }

            }
        }
    }
}
