package com.yozachar.apps.soundcraftmixer

import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.viewinterop.AndroidView
import androidx.webkit.WebSettingsCompat
import androidx.webkit.WebViewFeature

class WebLoader : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val targetPageUrl = intent.getStringExtra("TARGET_URL") ?: ""
        val errorPageUrl = "file:///android_asset/four-oh-four.html"
        setContent { GenericWebView(targetPageUrl, errorPageUrl) }
    }
}

fun pageLoadHelper(
    context: Context, webView: WebView, settings: WebSettings, pageUrl: String, features: Boolean
) {
    settings.apply {
        javaScriptEnabled = features
        domStorageEnabled = features
    }
    webView.clearCache(features)
    if (WebViewFeature.isFeatureSupported(WebViewFeature.ALGORITHMIC_DARKENING)) {
        WebSettingsCompat.setAlgorithmicDarkeningAllowed(settings, true)
    }
    webView.loadUrl(pageUrl)

    // this is a side-effect
    val sharedPref = context.getSharedPreferences("appData", Context.MODE_PRIVATE)
    val editor = sharedPref.edit()
    editor.putString("NEW_URL", pageUrl)
    editor.apply()
}

@Composable
fun GenericWebView(targetPageUrl: String, errorPageUrl: String) {
    val activePageUrl =
        if (getWiFiAddress(LocalContext.current).isNullOrEmpty()) errorPageUrl else targetPageUrl
    // Log.d("debug", "++++++++++++ aPU: $activePageUrl ++++++++++++")
    LocalView.current.keepScreenOn = true
    val cxt = LocalContext.current
    AndroidView(factory = {
        WebView(it).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
            )
            webViewClient = object : WebViewClient() {
                // override fun onPageStarted(
                //         view: WebView,
                //         url: String,
                //         favicon: Bitmap
                // ) {
                //     // TODO show you progress image
                //     super.onPageStarted(view, url, favicon)
                // }

                override fun shouldOverrideUrlLoading(
                    view: WebView?, request: WebResourceRequest?
                ): Boolean {
                    // return super.shouldOverrideUrlLoading(view, request)
                    return false // handles redirections within WebView
                }

                override fun onReceivedError(
                    view: WebView?, request: WebResourceRequest?, error: WebResourceError?
                ) {
                    super.onReceivedError(view, request, error)
                    // Log.d("debug", "++++++++++++ ePU: $errorPageUrl ++++++++++++")
                    pageLoadHelper(cxt, this@apply, settings, errorPageUrl, false)
                }

                // override fun onPageFinished(view: WebView, url: String) {
                //     // TODO hide your progress image
                //     super.onPageFinished(view, url)
                // }
            }
            pageLoadHelper(cxt, this, settings, activePageUrl, true)
        }
    }, update = { it.reload() })
}